#include POSTGRES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


// declare variables
char version[10] = "v0.1.0";
char settings_file[512];
struct global_config {
  int number_of_settings;
  struct citems {
    char name[64];
    char value[64];
  } config_items[64];
};
const char SETTINGS_STR[] = "user example\n\
passwd Pa55w0rd\ndb clock_times\nhost db.example.com\ninam 07:00:00\n\
outam 12:30:00\ninpm 13:00:00\noutpm 17:00:00";


// initialisation functions
void check_environment() {
  /* Ensure valid environment.
   * Check whether the `.clocker` file exists in the user's home directory, and
   * creates it if not.
   */
  FILE *settings;
  char buffer;
  sprintf(settings_file, "%s/.clocker", getenv("HOME"));

  if( access( settings_file, F_OK ) == -1 ) {
    printf("Initialising local config file. Please update `%s`.\n",
      settings_file);

    settings = fopen(settings_file, "w");
    fputs(SETTINGS_STR, settings);
    fclose(settings);
    exit(0);
  }
}


struct global_config initialise_config() {
  /* Load config settings.
   * Read settings file from user's local configuration file, and load it into
   * memory by way of a struct.
   */
  struct global_config gconfig = {};
  int n = 0;
  FILE *settings;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  char *setting_name, *setting_value;
  int number_of_settings;
  settings = fopen(settings_file, "r");
  if (settings == NULL)
    exit(1);
  while ((read = getline(&line, &len, settings)) != -1) {
    if (line[0] != '#' && line[0] != '\n') {
      setting_name = strtok(line, " ");
      sprintf(gconfig.config_items[n].name, settings_file);
      setting_value = strtok(NULL, " ");
      sprintf(gconfig.config_items[n].value, setting_value);
      n += 1;
    }
  }
  gconfig.number_of_settings = n;
  if (ferror(settings)) {
    /* handle error */
  }
  free(line);
  fclose(settings);

  return gconfig;
}


PGconn *create_db_connection() {
  int i;
  char conninfo[512];
  PGconn *dbconn;
  struct global_config gconfig = initialise_config();

  // set up connection
  sprintf(conninfo, "user=%s password=%s dbname=%s host=%s", \
    gconfig.config_items[0].value, \
    gconfig.config_items[1].value, \
    gconfig.config_items[2].value, \
    gconfig.config_items[3].value);

  // test database connection
  dbconn = PQconnectdb(conninfo);
  if (PQstatus(dbconn) == CONNECTION_BAD) {
    printf("Unable to connect to database\n");
    exit(1);
  }
  return dbconn;
};


// major functions
void help() {
  /* Display help message.
   */
  printf("Clocker.\n\
Clock in and out, record leave and manage time.\n\
\n\
Usage:\n\
  clocker -h | --help\n\
  clocker -v | --version\n\
  clocker -c | --clock [inam=<time>, outam=<time>, inpm=<time>, outpm=<time>]\n\
\n\
Options:\n\
  -h --help     Display this help message\n\
  -v --version  Display current version\n\
  -c --clock    Log clock times\n\
\n");
}


void test_suite() {
  /* Run test suites.
   */
  char query_string[512];
  char result_string[256];
  char test_string[256];
  char *test_data_clock[] = {
    "clocker"
    "--clock"
    "inpm='13:13:13'"
    "outpm='22:22:22'"
  };
  int old_stdout = dup(1);
  PGresult *query;
  PGconn *dbconn;
  struct global_config gconfig = initialise_config();

  printf("Running test suite.\n");

  /* PostgreSQL connection.
   * Check the connection by comparing the database name specified in the config
   * with the database name retrieved from the database itself.
   */
  printf("PostgreSQL Connection: ");

  // run test
  strcpy(test_string, gconfig.config_items[2].value);
  dbconn = create_db_connection();
  sprintf(query_string, "SELECT current_database();");
  query = PQexec(dbconn, query_string);
  strcpy(result_string, PQgetvalue(query, 0, 0));
  strcat(result_string, "\n");

  // check result
  if(!strcmp(result_string, test_string)) {
    printf("Success\n");
  } else {
    printf("Failure\n");
  }

  /* Clock function.
   * Run a clock function with two parameters specified, then clear up the input
   * afterwards. Only supplying two parameters forces the use of the config
   * file, and allows an unusual time to be specified easing the tidy up after.
   * Output has been redirected to /dev/null.
   */
  printf("Clock function: ");

  // run test
  freopen ("/dev/null", "w", stdout);
  clock(4, test_data_clock);
  fclose(stdout);
  stdout = fdopen(old_stdout, "w");
  strcpy(test_string, "22:22:22");

  // check db
  sprintf(query_string, "SELECT outpm FROM timecards\
  ORDER BY date DESC LIMIT 1;");
  query = PQexec(dbconn, query_string);
  strcpy(result_string, PQgetvalue(query, 0, 0));
  if(!strcmp(result_string, test_string)) {
    printf("Success\n");
  } else {
    printf("Failure\n");
  }

  // tidy up
  sprintf(query_string, "DELETE FROM timecards WHERE outpm='22:22:22'");
  query = PQexec(dbconn, query_string);
}


// main functions
int clock(int argc, char *argv[]) {
  /* Clock times.
   * Log times into database using defaults where appropriate.
   */
  char query_string[1024];
  char inam[64];
  char outam[64];
  char inpm[64];
  char outpm[64];
  char token_key[64];
  char token_value[64];
  char *token;
  int args_count;
  int n;
  PGresult *query;
  PGconn *dbconn;
  struct global_config gconfig = initialise_config();

  dbconn = create_db_connection();

  // validate arguments
  if(argc > 6) {
    printf("Incorrect number of arguments specified. See `clocker --help` for \
more information.\n");
    exit(1);
  }
  args_count = argc - 2;

  // setup data structures
  strcpy(inam, gconfig.config_items[4].value);
  strtok(inam, "\n");
  strcpy(outam, gconfig.config_items[5].value);
  strtok(outam, "\n");
  strcpy(inpm, gconfig.config_items[6].value);
  strtok(inpm, "\n");
  strcpy(outpm, gconfig.config_items[7].value);
  strtok(outpm, "\n");

  for  (n = 0; n < args_count; n++) {
    token = strtok(argv[ n + 2 ], "=");
    strcpy(token_key, token);
    token = strtok(NULL, "=");
    strcpy(token_value, token);

    if (!strcmp(token_key, "inam")) {
      strcpy(inam, token_value);
    } else if (!strcmp(token_key, "outam")) {
      strcpy(outam, token_value);
    } else if (!strcmp(token_key, "inpm")) {
      strcpy(inpm, token_value);
    } else if (!strcmp(token_key, "outpm")) {
      strcpy(outpm, token_value);
    }
  }

  // update database
  sprintf(query_string, "INSERT INTO timecards VALUES \
    (CURRENT_DATE, '%s', '%s', '%s', '%s');", inam, outam, inpm, outpm);
  query = PQexec(dbconn, query_string);

  // pull down return data
  sprintf(query_string, "SELECT * FROM timecards ORDER BY date DESC LIMIT 1;");
  query = PQexec(dbconn, query_string);
  printf("Latest log:\n%s %s %s %s %s\n\n", PQgetvalue(query, 0, 0), \
    PQgetvalue(query, 0, 1), PQgetvalue(query, 0, 2), PQgetvalue(query, 0, 3), \
    PQgetvalue(query, 0, 4));
  sprintf(query_string, "SELECT \
    (SELECT (SUM(outam) - SUM(inam)) \
      + (SUM(outpm) - SUM(inpm)) \
      - SUM(INTERVAL '7 hours 24 minutes') \
      FROM timecards WHERE date < now()) \
    - (SELECT SUM(INTERVAL '7 hours 24 minutes') FILTER (WHERE type = 'flexi') \
      + SUM(INTERVAL '3 hours 42 minutes') FILTER (WHERE type = 'flexihalf') \
      - SUM(INTERVAL '3 hours 42 minutes') FILTER (WHERE type = 'annualhalf') \
      FROM leave WHERE date < now()) \
    + (SELECT SUM(amount) FROM adjustments \
      WHERE adjustment in ('flexi', 'duty', 'travel') AND date < now() \
      );");
  query = PQexec(dbconn, query_string);
  printf("Current flexi:\n%s (H:M:S)\n", PQgetvalue(query, 0, 0));

}


int main(int argc, char *argv[]) {
  int n;
  // validate arguments
  if(argc == 1) {
    printf("At least one option required. See `clocker --help` for more \
information.\n");
    exit(1);
  };
  check_environment();

  // read arguments and functions
  if(!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
    help();
  } else if(!strcmp(argv[1], "-v") || !strcmp(argv[1], "--version")) {
    printf("Version: %s\n", version);
  } else if(!strcmp(argv[1], "-t") || !strcmp(argv[1], "--testing")) {
    test_suite();
  } else if(!strcmp(argv[1], "-c") || !strcmp(argv[1], "--clock")) {
    clock(argc, argv);
  }
  return 0;
}
