![TheOrganist24 Code](https://hosted.courtman.me.uk/img/logos/theorganist24_banner_code.png "TheOrganist24 Code")

# Clocker
> Clock in and out, record leave and manage time

## Installation
1. `make`
2. `mv bin/clocker /usr/local/bin/`

## Using
Usage:
* clocker -h | --help
* clocker -v | --version
* clocker -t | --testing **Not yet enabled**
* clocker -c | --clock [inam=<time>, outam=<time>, inpm=<time>, outpm=<time>]
* clocker -l | --leave [<type>] ['<note>'] ['<date>' ('<end-date>')] **Not yet enabled**
* clocker -s | --sick [<note>] ['<date>' ('<end-date>')] **Not yet enabled**
* clocker -r | --report **Not yet enabled**
* clocker -f | --full-report (annual|monthly) [<year> <month>] [(-o |--output=)(screen|pdf|html)] **Not yet enabled**

Options:
* -h --help         Display this help message
* -v --version      Display current version
* -t --testing      Run a test suite
* -c --clock        Clock in with default times, or specific (format eg. '12:30:00')
* -l --leave        Add leave dates with type, accounts for weekends, but not public holidays
* -s --sick         Add sick leave, accounts for weekends, but not public holidays
* -r --report       Generate a short report
* -f --full-report  Generate annual or monthly report [default: current month, or year-to-date]
* -o --output       Output style [default: screen]


## Acknowledgements
* [Hiltmon](https://hiltmon.com/blog/2013/07/03/a-simple-c-plus-plus-project-structure/)
