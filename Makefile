CC			:= gcc
CCFLAGS := -lpq

TARGET	:= bin/clocker
SRC			:= src/clocker.c

UNAME := $(shell uname -s)
ifeq ($(UNAME),Linux)
  CCFLAGS += -D POSTGRES='<postgresql/libpq-fe.h>'
endif
ifeq ($(UNAME),Darwin)
  CCFLAGS += -D POSTGRES='<libpq-fe.h>'
endif

all: $(TARGET)

clean:
	rm -f $(TARGET)

$(TARGET): $(SRC)
	$(CC) $(CCFLAGS) $(SRC) -o $(TARGET)

